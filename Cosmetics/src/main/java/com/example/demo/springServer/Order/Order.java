package com.example.demo.springServer.Order;

import com.example.demo.springServer.Product.Product;
import com.example.demo.springServer.User.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Order {

    private static AtomicLong idCounter = new AtomicLong();
    private Date date;
    private User user;
    private List<Product> products;

    public Order(User user, Date date){
        createID();
        this.user = user;
        this.date = date;
        this.products = new ArrayList<>();
    }

    public static String createID()
    {
        return String.valueOf(idCounter.getAndIncrement());
    }

    public static AtomicLong getIdCounter() {
        return idCounter;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Product> getProducts() {
        return products;
    }
}
