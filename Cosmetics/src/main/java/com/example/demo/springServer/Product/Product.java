package com.example.demo.springServer.Product;

import com.example.demo.springServer.User.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Product {

    private String category;
    private static AtomicLong idCounter = new AtomicLong();
    private String name;
    private double amount;
    private List<User> waitingList;

    public Product(String category, String name, double amount){
        createID();
        this.category = category;
        this.name = name;
        this.amount = amount;
    }

    public static String createID()
    {
        return String.valueOf(idCounter.getAndIncrement());
    }

    public static AtomicLong getIdCounter() {
        return idCounter;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
