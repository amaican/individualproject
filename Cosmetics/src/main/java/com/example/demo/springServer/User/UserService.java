package com.example.demo.springServer.User;

import com.example.demo.springServer.Account.Account;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserService {

    private List<User> users = new ArrayList<>();

    public void addUsers(){
        User user1 = new User(new Account("jason@gmail.com","jason","springBootTry"), "Jason Donavon", "Lombokpad 32");
        User user2 = new User(new Account("medison@gmail.com","madison", "tryAgain"), "Madison Forbs", "Pisanostraat 49");
        User user3 = new User(new Account("ivy@gmail.com", "ivy", "adminPassword"), "Ivy Dickens", "PalmStreet 31");
        users.add(user1);
        users.add(user2);
        user3.setRole("administrator");
        users.add(user3);
    }

    public void addUser(User user){
        users.add(user);
    }

    public List<User> getAllUsers(){
        addUsers();
        return this.users;
    }

    public User getUserByRole(String role){
        return users.stream().filter(user -> user.getRole().equals(role)).findFirst().get();
    }

}
