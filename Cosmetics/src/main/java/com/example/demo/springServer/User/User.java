package com.example.demo.springServer.User;

import com.example.demo.springServer.Account.Account;
import com.example.demo.springServer.Order.Order;
import com.example.demo.springServer.Product.Product;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class User {

    private String key;
    private Account account;
    private String role;
    private String name;
    private String address;
    private List<Order> orders;
    private List<Product> routine;

    public User(Account acc, String name, String address){
        this.key = UUID.randomUUID().toString();
        this.account = acc;
        this.name = name;
        this.address = address;
        this.role = "customer";
    }

    public String getKey() {
        return key;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
